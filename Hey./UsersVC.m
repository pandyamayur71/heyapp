//
//  UsersVC.m
//  Hey.
//
//  Created by Mayur Pandya on 05/07/14.
//  Copyright (c) 2014 Mayur Pandya. All rights reserved.
//

#import "UsersVC.h"


//#define PUSH_URL "http://imobsoftsolutions.in/ws/heyapp/index.php/api/users/sendMessage?invite_username=%@&username=%@&sound=%@"
#define loggedUser [[NSUserDefaults standardUserDefaults] valueForKey:@"username"]
@interface UsersVC ()
{
    
}
@end

@implementation UsersVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(addUser:) name:@"addUser" object:nil];
    
   /* [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;*/
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navBar"] forBarMetrics:UIBarMetricsDefault];
    
    self.title = @"Friends";
    
    UILabel *nav_titlelbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200,40)];
    nav_titlelbl.text=self.navigationItem.title;;
    nav_titlelbl.textColor=[UIColor whiteColor];
    nav_titlelbl.textAlignment=NSTextAlignmentCenter;
    nav_titlelbl.backgroundColor =[UIColor clearColor];
    nav_titlelbl.adjustsFontSizeToFitWidth=YES;
    UIFont *lblfont=[UIFont systemFontOfSize:25];
    [nav_titlelbl setFont:lblfont];
    self.navigationItem.titleView=nav_titlelbl;
    
    self.items = [NSMutableArray new];
    
    
    UIView *footerView  = [[UIView alloc] init];
    
    [footerView setFrame:CGRectMake(0, 0, 300, 100)];
    
    UIButton *addNewBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [addNewBtn setFrame:CGRectMake(10, 5, 300, 33)];
    [addNewBtn setBackgroundImage:[UIImage imageNamed:@"btnInviteNetwork"] forState:UIControlStateNormal];
    [addNewBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [addNewBtn addTarget:self action:@selector(invite) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:addNewBtn];
    ///
    UIButton *addNewBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [addNewBtn1 setFrame:CGRectMake(10, 45, 300, 33)];
    [addNewBtn1 setBackgroundImage:[UIImage imageNamed:@"btnInviteUser"] forState:UIControlStateNormal];
    [addNewBtn1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [addNewBtn1 addTarget:self action:@selector(inviteUser) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:addNewBtn1];
    
    self.tableView.tableFooterView = footerView;
    
    footerView = nil;
    
    [self addBarButton];
    
    [self.view setBackgroundColor: [self colorWithHexString:@"f2f2f2"]];
}

-(void)addUser:(NSNotification*)notification
{
    //NSLog(@"%@",notification.object);
    
    NSString *login = [[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
    
    
     /*   [self.view setUserInteractionEnabled:FALSE];
        
        actView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        actView.center = self.view.center;
        actView.color = [self colorWithHexString:@"f4713c"];
        [self.view addSubview:actView];
        [actView startAnimating];*/
    
        [appDelegate startActivityIndicator:appDelegate.window];
    
        NSString *postString =[NSString stringWithFormat:@"username=%@&invity_username=%@",login,notification.object];
        
        //NSLog(@"%@",[postString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]);
        
        NSURL *aUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/InviteUser",jsonURL]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:aUrl cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
        
        
        
        
        
        [request setHTTPMethod:@"POST"];
        
        
        NSData *parameterData = [postString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        
        
        
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[parameterData length]];
        
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:parameterData];
        
      //  NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [appDelegate stopActivityIndicator];
    
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
         {
            // NSLog(@"Response = %@",response.description);
             
             NSString* myString;
             
             myString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
             
            // NSLog(@"Data = %@",myString);
             
             NSDictionary *jsonObject=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
             
             if([[jsonObject valueForKey:@"status"] isEqualToString:@"1"])
             {
                /* self.view.userInteractionEnabled = TRUE;
                 [actView stopAnimating];
                 */
                 [self.items addObject:[jsonObject objectForKey:@"user_detail"]];
                 [self.tableView reloadData];

              //    [self loadData];
                 
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hey!" message:@"User added successfully." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                 
                 [alert show];
                 
                 alert = nil;
                 
                 
             }
             else
             {
              /*   self.view.userInteractionEnabled = TRUE;
                 [actView stopAnimating];
                 [actView removeFromSuperview];*/
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hey!" message:[jsonObject valueForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                 [alert show];
             }
             
         }];
        

    
    
    
}

-(void)addBarButton
{
    
    UIImage *imgBack=[UIImage imageNamed:@"imgSettings.png"];
    
    UIButton *btnSettings = [[UIButton alloc] initWithFrame:CGRectMake(0, 15,20,20)];
    [btnSettings setImage:imgBack forState:UIControlStateNormal];
    //    [backButton setShowsTouchWhenHighlighted:TRUE];
    [btnSettings addTarget:self action:@selector(dispalyMore) forControlEvents:UIControlEventTouchDown];
    UIBarButtonItem *rightBar = [[UIBarButtonItem alloc] initWithCustomView:btnSettings];
    self.navigationItem.rightBarButtonItem = rightBar;
    
    
}

-(void) dispalyMore
{
    SettingsVC *objSVC = [[SettingsVC alloc]initWithNibName:@"SettingsVC" bundle:nil];
    
    [self.navigationController pushViewController:objSVC animated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = FALSE;
    
    self.navigationItem.leftBarButtonItem = nil;
    
    self.navigationItem.hidesBackButton = YES;
    
    [appDelegate startActivityIndicator:appDelegate.window];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.view.userInteractionEnabled = FALSE;
        
        [self loadData];
    });
    
    
}

-(void)loadData
{
    
    
    if([self.items count]>0)
        [self.items removeAllObjects];
    
    if([self.selectedIndexPaths count]>0)
        [self.selectedIndexPaths removeAllObjects];
    
    NSMutableArray *temp = [appDelegate getUserList];  //ws_getCategories
    
    [self.items addObjectsFromArray:temp];
    
    [self.tableView reloadData];
    
    self.view.userInteractionEnabled = TRUE;
    
    [appDelegate stopActivityIndicator];
}

-(void)invite
{
    
    //NSLog(@"%@",[NSString stringWithFormat:@"<h3>Add Me to your network</h3><a href='hey://%@'>Add Me !</a>",[[NSUserDefaults standardUserDefaults] valueForKey:@"username"] ]);
    
    
    UIActionSheet *optionSheet = [[UIActionSheet alloc]initWithTitle:@"Invite User Via:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Message",@"Mail", nil];
    
    [optionSheet showInView:self.view];
    
 //   [self inviteByTwitter];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0)
    {
        [self inviteByMessage];
    }
    else if (buttonIndex == 1)
    {
        [self inviteByMail];
    }
}

-(void)inviteByMessage
{
    if(![MFMessageComposeViewController canSendText])
    {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Hey!" message:@"Your device doesn't support SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    
    NSString *file = [[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
    
    NSString *message = [NSString stringWithFormat:@"Add me in your Hey! App network. My username is %@. Please add me!", file];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setSubject:@"Hey!"];
    [messageController setBody:message];
    
    // Present message view controller on screen
    [self presentViewController:messageController animated:YES completion:nil];
        
    
    
}

-(void)inviteByMail
{
    MFMailComposeViewController *mailVC = [[MFMailComposeViewController alloc] init];
    mailVC.mailComposeDelegate = self;
    if([MFMailComposeViewController canSendMail])
    {
        [mailVC setSubject:@"Hey!"];
        [mailVC setToRecipients:nil];
        
        [mailVC setMessageBody: [NSString stringWithFormat:@"<h3>Add me in your Hey! App network. My username is %@. Please add me!</h3><a href='hey://%@'>Add Me !</a>",[[NSUserDefaults standardUserDefaults] valueForKey:@"username"],[[NSUserDefaults standardUserDefaults] valueForKey:@"username"] ] isHTML: YES];
        
        //  [mailVC setMessageBody:@"Enjoy HD WallPaper" isHTML:NO];
        //     NSString *imageURL = [dict objectForKey:@"master_image"];
        //     NSURL *url = [NSURL URLWithString:imageURL];
        /*     NSURL *url = [NSURL URLWithString:[dict objectForKey:@"master_image"]];
         AsyncImageView *newPageView = [[AsyncImageView alloc] init];
         newPageView.imageURL = url;
         //        [newPageView loadImageFromURL:url withName:@"" andType:@"jpg"];
         newPageView.contentMode = UIViewContentModeScaleAspectFit;
         */
      //  [mailVC addAttachmentData:UIImagePNGRepresentation(asyncImgView.image) mimeType:@"image/jpg" fileName:[NSString stringWithFormat:@"HD_WallPapaer.jpg"]];
        
        
        [self presentViewController:mailVC animated:YES completion:nil];
        
    }

}

#pragma mark - MailCompose Delegate

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    
    switch (result)
    {
        case MFMailComposeResultCancelled:
            ////NSlog(@"Result: canceled");
            break;
            
        case MFMailComposeResultSaved:
            ////NSlog(@"Result: saved");
            break;
            
        case MFMailComposeResultSent:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hey!" message:@"Mail Successfully Sent." delegate:Nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            //  NSString* myLogEvent = @"Image has been Sent using Email";
            //emailAddress.text = @"";
            break;
        }
        case MFMailComposeResultFailed:
            ////NSlog(@"Result: failed");
            break;
            
        default:
            ////NSlog(@"Result: not sent");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma mark -
#pragma mark - Message Methods

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
	switch (result)
    {
		case MessageComposeResultCancelled:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Cancel" message:@"SMS has been cancelled... !!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alert show];
            alert = nil;
            
            break;
            
        }
		case MessageComposeResultFailed:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failed" message:@"SMS has been Failed... !!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alert show];
            alert = nil;
            break;
        }
		case MessageComposeResultSent:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success" message:@"SMS has been sent successfully... !!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alert show];
            alert = nil;
            
            break;
        }
		default:
			break;
	}
    
	[self dismissViewControllerAnimated:YES completion:nil];
}

-(void)inviteUser
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Enter Username to invite." message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    
    
    txtField = [[UITextField alloc] init];
    txtField.frame = CGRectMake(12, 40, 260, 22);
    txtField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    txtField.autocorrectionType = UITextAutocorrectionTypeNo;
    txtField.backgroundColor = [UIColor whiteColor];
    txtField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    txtField.placeholder = @"Username";
    txtField.keyboardType = UIKeyboardTypeDefault;
    txtField.tag = 1;
    txtField.delegate = self;
    [alert addSubview:txtField];
    
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert textFieldAtIndex:0].keyboardType = UIKeyboardTypeDefault;
    
    alert.tag = 1;
    [alert show];
    
    alert = nil;
}

#pragma mark -
#pragma mark - Alert View Methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1)
    {
        if(buttonIndex == 1)
        {
            NSString *inputText = [[alertView textFieldAtIndex:0] text];
            
            if([inputText length] > 0)
            {
              /*  self.view.userInteractionEnabled = FALSE;
                
                actView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
                actView.center = self.view.center;
                actView.color = [self colorWithHexString:@"f4713c"];
                [self.view addSubview:actView];
                [actView startAnimating];
                */
                
                [appDelegate startActivityIndicator:appDelegate.window];
                NSString *postString =[NSString stringWithFormat:@"username=%@&invity_username=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"username"],inputText];
                
                //NSLog(@"%@",[postString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]);
                
                NSURL *aUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/InviteUser",jsonURL]];
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:aUrl cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
                
                
                
                
                
                [request setHTTPMethod:@"POST"];
                
                
                NSData *parameterData = [postString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
                
                
                
                NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[parameterData length]];
                
                [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
                [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
                [request setHTTPBody:parameterData];
                
             //   NSOperationQueue *queue = [[NSOperationQueue alloc] init];
                
                
                [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
                 {
                     
                     [appDelegate stopActivityIndicator];
                     //NSLog(@"Response = %@",response.description);
                     
                     NSString* myString;
                     
                     myString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                     
                     //NSLog(@"Data = %@",myString);
                     
                     NSDictionary *jsonObject=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
                     
                     if([[jsonObject valueForKey:@"status"] isEqualToString:@"1"])
                     {
                       /*  self.view.userInteractionEnabled = TRUE;
                         [actView stopAnimating];
                         */
                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hey!" message:[NSString stringWithFormat:@"%@ added successfully.",inputText] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                         
                         [alert show];
                         
                         alert = nil;
                         
                         [self.items addObject:[jsonObject objectForKey:@"user_detail"]];
                         [self.tableView reloadData];
                         
                      //   [self loadData];
                         
                      
                         
                         
                     }
                     else
                     {
                        /* self.view.userInteractionEnabled = TRUE;
                         [actView stopAnimating];
                         */
                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hey!" message:@"Invalid username or username already exist." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                         [alert show];
                     }
                     
                 }];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hey!" message:@"Please enter username." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                [alert show];
                alert = nil;
            }
        }
    }
}


#pragma mark -
#pragma mark - Table View Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  [self.items count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    BOOL isSelected = [self.selectedIndexPaths containsObject:indexPath];
    

    if(isSelected == TRUE)
    {
        return 160;
    }
    else
    {
        return 44;
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CellIdentifier";
    
    SMTableViewCell *cell= (SMTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        {
        
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SMTableViewCell" owner:nil options:nil] lastObject];
        }
        else
        {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SMTableViewCellSeven" owner:nil options:nil] lastObject];
        }
    }
    
    
    
    BOOL isSelected = [self.selectedIndexPaths containsObject:indexPath];
    //cell.statusLabel.numberOfLines = isSelected?0:2;
    
    //NSString *text = self.items[indexPath.row];
    
    if([[[self.items objectAtIndex:indexPath.row] valueForKey:@"status"] isEqualToString:@"active"])
    {
        cell.statusLabel.text = [[self.items objectAtIndex:indexPath.row] valueForKey:@"username"];
        
        cell.statusLabel.font = [UIFont boldSystemFontOfSize:20.0];
        cell.statusLabel.textColor = [self colorWithHexString:@"f4713c"];
        //cell.statusLabel.backgroundColor = [UIColor clearColor];
    }
    
    
    
    if(isSelected == FALSE)
    {
       /* cell.btnHeyM.hidden = TRUE;
        cell.btnGdMornM.hidden = TRUE;
        cell.btnGnM.hidden = TRUE;
        cell.btnLoveM.hidden = TRUE;
        cell.btnGBM.hidden = TRUE;
        
        cell.btnHeyW.hidden = TRUE;
        cell.btnGdMornW.hidden = TRUE;
        cell.btnGnW.hidden = TRUE;
        cell.btnLoveW.hidden = TRUE;
        cell.btnGBW.hidden = TRUE;
        
        cell.btnBlockUser.hidden = TRUE;
        cell.btnDeleteUser.hidden =TRUE;
        
        cell.btnMan.hidden = TRUE;
        cell.btnWoman.hidden =TRUE;
        
        cell.imgLine1.hidden = TRUE;
        cell.imgLine2.hidden = TRUE;
        cell.imgLine3.hidden = TRUE;*/
        
        cell.bgView.hidden = TRUE;
        
        cell.verticalSepLine.constant = 1;
        
       // cell.expandHeightConstant.constant = 0;
    }
    else
    {
        cell.verticalSepLine.constant = 122;
    }
    
       //***
    
    // For Man
    [cell.btnHeyM addTarget:self action:@selector(btnHeyMClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnGdMornM addTarget:self action:@selector(btnGdMornMClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnGnM addTarget:self action:@selector(btnGnMClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnLoveM addTarget:self action:@selector(btnLoveMClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnGBM addTarget:self action:@selector(btnGBMClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    // For Woman
    [cell.btnHeyW addTarget:self action:@selector(btnHeyWClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnGdMornW addTarget:self action:@selector(btnGdMornWClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnGnW addTarget:self action:@selector(btnGnWClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnLoveW addTarget:self action:@selector(btnLoveWClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnGBW addTarget:self action:@selector(btnGBWClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    cell.btnHeyM.tag = cell.btnGdMornM.tag = cell.btnGnM.tag = cell.btnLoveM.tag = cell.btnGBM.tag = indexPath.row;
    cell.btnHeyW.tag = cell.btnGdMornW.tag = cell.btnGnW.tag = cell.btnLoveW.tag = cell.btnGBW.tag = indexPath.row;
    
    
    // delete and block user
    
    [cell.btnBlockUser addTarget:self action:@selector(btnBlockUserClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnBlockUser setTag:indexPath.row];
    
    [cell.btnDeleteUser addTarget:self action:@selector(btnDeleteUserClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnDeleteUser setTag:indexPath.row];
    //***
    
    
    /*NSString *buttonTitle = isSelected?@"See Less":@"See More";
    [cell.seeMoreButton setTitle:buttonTitle forState:UIControlStateNormal];
    [cell.seeMoreButton addTarget:self action:@selector(seeMoreButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [cell.seeMoreButton setTag:indexPath.row];*/
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self addOrRemoveSelectedIndexPath:indexPath];
}

#pragma mark -
#pragma mark - Custom Methods


- (void)seeMoreButtonPressed:(UIButton *)button
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:button.tag inSection:0];
    
    [self addOrRemoveSelectedIndexPath:indexPath];
}

- (void)addOrRemoveSelectedIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (!self.selectedIndexPaths)
    {
        self.selectedIndexPaths = [NSMutableArray new];
    }
    
    
    BOOL containsIndexPath = [self.selectedIndexPaths containsObject:indexPath];
    
    if (containsIndexPath)
    {
        [self.selectedIndexPaths removeObject:indexPath];
    }
    else
    {
        [self.selectedIndexPaths removeAllObjects];
        [self.selectedIndexPaths addObject:indexPath];
        
    }
    [self.tableView reloadData];
    
    //[self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    
}


// block and delete user

-(void)btnBlockUserClicked:(id)sender
{
    indexpath = [sender tag];
    
  /*  self.view.userInteractionEnabled = FALSE;
    
    actView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    actView.center = self.view.center;
    actView.color = [self colorWithHexString:@"f4713c"];
    [self.view addSubview:actView];
    [actView startAnimating];
    */
    
    [appDelegate startActivityIndicator:appDelegate.window];
    
    NSString *postString =[NSString stringWithFormat:@"username=%@&invity_username=%@&blocked=1",[[NSUserDefaults standardUserDefaults] valueForKey:@"username"],[[self.items objectAtIndex:indexpath] valueForKey:@"username"]];
    
   // NSLog(@"%@",[postString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]);
    
    NSURL *aUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/blockUnblock",jsonURL]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:aUrl cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    
    
    
    
    [request setHTTPMethod:@"POST"];
    
    
    NSData *parameterData = [postString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    
    
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[parameterData length]];
    
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:parameterData];
    
  //  NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
        // NSLog(@"Response = %@",response.description);
         
         NSString* myString;
         
         myString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
         
         //NSLog(@"Data = %@",myString);
         
         NSDictionary *jsonObject=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
         [appDelegate stopActivityIndicator];

         if([[jsonObject valueForKey:@"status"] isEqualToString:@"1"])
         {
           //  self.view.userInteractionEnabled = TRUE;
          //   [actView stopAnimating];
             
             
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hey!" message:[NSString stringWithFormat:@"%@ has been blocked successfully.",[[self.items objectAtIndex:indexpath] valueForKey:@"username"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
             
             [alert show];
             
             alert = nil;
             
             [self.items removeObjectAtIndex:indexpath];
             [self.selectedIndexPaths removeAllObjects];
             [self.tableView reloadData];
             //[self loadData];

         }
         else
         {
           //  self.view.userInteractionEnabled = TRUE;
         //    [actView stopAnimating];
             
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hey!" message:@"Invalid username. Try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
         }
         
     }];
}

-(void)btnDeleteUserClicked:(id)sender
{
    
    indexpath = [sender tag];
    
   /* self.view.userInteractionEnabled = FALSE;
    
    actView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    actView.center = self.view.center;
    [self.view addSubview:actView];
    [actView startAnimating];
  */
    
    
    [appDelegate startActivityIndicator:appDelegate.window];
    NSString *postString =[NSString stringWithFormat:@"username=%@&invity_username=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"username"],[[self.items objectAtIndex:indexpath] valueForKey:@"username"]];
    
   // NSLog(@"%@",[postString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]);
    
    NSURL *aUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/remove",jsonURL]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:aUrl cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    
    
    
    
    [request setHTTPMethod:@"POST"];
    
    
    NSData *parameterData = [postString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    
    
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[parameterData length]];
    
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:parameterData];
    
  //  NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
         //NSLog(@"Response = %@",response.description);
         
         NSString* myString;
         
         myString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
         
         //NSLog(@"Data = %@",myString);
         
         NSDictionary *jsonObject=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
         [appDelegate stopActivityIndicator];
         if([[jsonObject valueForKey:@"status"] isEqualToString:@"1"])
         {
           /*  self.view.userInteractionEnabled = TRUE;
             [actView stopAnimating];
             */

             
                             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hey!" message:[NSString stringWithFormat:@"%@ has been deleted successfully.",[[self.items objectAtIndex:indexpath] valueForKey:@"username"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
             
             [alert show];
             
             alert = nil;
             
             
             
              //   [appDelegate stopActivityIndicator];

                 [self.items removeObjectAtIndex:indexpath];
                 [self.selectedIndexPaths removeAllObjects];
                 [self.tableView reloadData];

             
                        //[self loadData];
             
         }
         else
         {
          /*   self.view.userInteractionEnabled = TRUE;
             [actView stopAnimating];
            */
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hey!" message:@"Invalid username. Try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
         }
         
     }];
    
}
// For Man

-(void)btnHeyMClicked:(UIButton *)sender
{
  //  [[NSUserDefaults standardUserDefaults] valueForKey:@"username"]
    [self sendMessagetoUser:[sender tag] withSound:@"Hey_M.caf" andAlertis:[NSString stringWithFormat:@"%@ saying Hey to You!",[[NSUserDefaults standardUserDefaults] valueForKey:@"username"]]];
    
  /*  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hey :)" message:@"Button Hey Clicked (Man Voice)." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];*/
}

-(void)btnGdMornMClicked:(UIButton *)sender

{
    
    
    [self sendMessagetoUser:[sender tag] withSound:@"GM_M.caf" andAlertis:[NSString stringWithFormat:@"%@ saying Good Morning to You!",[[NSUserDefaults standardUserDefaults] valueForKey:@"username"]]];

  /*  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hey :)" message:@"Button Hey Good Morning Clicked (Man Voice)." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];*/
}

-(void)btnGnMClicked:(UIButton *)sender
{
    
    [self sendMessagetoUser:[sender tag] withSound:@"GN_M.caf" andAlertis:[NSString stringWithFormat:@"%@ saying Good Night to You!",[[NSUserDefaults standardUserDefaults] valueForKey:@"username"]]];

    
  /*  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hey :)" message:@"Button Hey Good Night Clicked (Man Voice)." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];*/
}

-(void)btnLoveMClicked:(UIButton *)sender

{
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"RemoveAdsValue"])
    {
        [self sendMessagetoUser:[sender tag] withSound:@"LOVE_M.caf" andAlertis:[NSString stringWithFormat:@"%@ saying Love You!",[[NSUserDefaults standardUserDefaults] valueForKey:@"username"]]];

    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hey!" message:@"Please purchase extra voices from settings." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}
-(void)btnGBMClicked:(UIButton *)sender

{
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"RemoveAdsValue"])
    {
        [self sendMessagetoUser:[sender tag] withSound:@"GB_M.caf" andAlertis:[NSString stringWithFormat:@"%@ saying Good Bye to You!",[[NSUserDefaults standardUserDefaults] valueForKey:@"username"]]];

    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hey!" message:@"Please purchase extra voices from settings." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

// For Woman


-(void)btnHeyWClicked:(UIButton *)sender

{
    
    [self sendMessagetoUser:[sender tag] withSound:@"Hey_W.caf" andAlertis:[NSString stringWithFormat:@"%@ saying Hey to You!",[[NSUserDefaults standardUserDefaults] valueForKey:@"username"]]];

  /*  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hey :)" message:@"Button Hey Clicked (Woman Voice)." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];*/
}

-(void)btnGdMornWClicked:(UIButton *)sender

{
    
    [self sendMessagetoUser:[sender tag] withSound:@"GM_W.caf" andAlertis:[NSString stringWithFormat:@"%@ saying Good Morning to You!",[[NSUserDefaults standardUserDefaults] valueForKey:@"username"]]];

 /*   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hey :)" message:@"Button Hey Good Morning Clicked (Woman Voice)." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show]; */
}

-(void)btnGnWClicked:(UIButton *)sender

{
    
    [self sendMessagetoUser:[sender tag] withSound:@"GN_W.caf" andAlertis:[NSString stringWithFormat:@"%@ saying Good Night to You!",[[NSUserDefaults standardUserDefaults] valueForKey:@"username"]]];

  /*  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hey :)" message:@"Button Hey Good Night Clicked (Woman Voice)." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show]; */
}

-(void)btnLoveWClicked:(UIButton *)sender

{
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"RemoveAdsValue"])
    {
        [self sendMessagetoUser:[sender tag] withSound:@"LOVE_W.caf" andAlertis:[NSString stringWithFormat:@"%@ saying Love You!",[[NSUserDefaults standardUserDefaults] valueForKey:@"username"]]];
 
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hey!" message:@"Please purchase extra voices from settings." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}
-(void)btnGBWClicked:(UIButton *)sender

{
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"RemoveAdsValue"])
    {
        [self sendMessagetoUser:[sender tag] withSound:@"GB_W.caf" andAlertis:[NSString stringWithFormat:@"%@ saying Good Bye to You!",[[NSUserDefaults standardUserDefaults] valueForKey:@"username"]]];

    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hey!" message:@"Please purchase extra voices from settings." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void) sendMessagetoUser:(NSInteger )tag withSound:(NSString *)sound andAlertis:(NSString *)alert
{
    
    
    //#define PUSH_URL "http://imobsoftsolutions.in/ws/heyapp/index.php/api/users/sendMessage?invite_username=%@&username=%@&sound=%@"

    
 /*   self.view.userInteractionEnabled = FALSE;
    
    actView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    actView.center = self.view.center;
    actView.color = [self colorWithHexString:@"f4713c"];
    [self.view addSubview:actView];
    [actView startAnimating];
    */
    
    [appDelegate startActivityIndicator:appDelegate.window];
    NSString *invite_user = [[self.items objectAtIndex:tag] valueForKey:@"username"];
    
    NSString *postString =[NSString stringWithFormat:@"username=%@&invity_username=%@&sound=%@&alert=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"username"],invite_user,sound,alert];
    
    //NSLog(@"%@",[postString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]);
    
    NSURL *aUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/sendMessage",jsonURL]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:aUrl cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    
    
    
    
    [request setHTTPMethod:@"POST"];
    
    
    NSData *parameterData = [postString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    
    
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[parameterData length]];
    
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:parameterData];
    
   // NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
         //NSLog(@"Response = %@",response.description);
         
         NSString* myString;
         
         myString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
         
         //NSLog(@"Data = %@",myString);
         
         NSDictionary *jsonObject=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
         
         [appDelegate stopActivityIndicator];
         if([[jsonObject valueForKey:@"status"] isEqualToString:@"1"])
         {
             
            /* UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hey!" message:@"User has been blocked successfully." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
             
             [alert show];
             
             alert = nil;*/
             
             
         }
         else
         {
           /*  self.view.userInteractionEnabled = TRUE;
             [actView stopAnimating];
             
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hey!" message:@"Invalid username. Try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show]; */
         }
         
     }];

    
}

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
