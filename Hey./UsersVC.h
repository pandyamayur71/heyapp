//
//  UsersVC.h
//  Hey.
//
//  Created by Mayur Pandya on 05/07/14.
//  Copyright (c) 2014 Mayur Pandya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UsersVC : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,MFMailComposeViewControllerDelegate,UIActionSheetDelegate,MFMessageComposeViewControllerDelegate>
{
    
    UITextField  *txtField;
    UIActivityIndicatorView *actView;
    
    int indexpath;
    
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) NSMutableArray *selectedIndexPaths;

@end
