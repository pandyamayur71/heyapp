//
//  AboutVC.h
//  Hey.
//
//  Created by Mayur Pandya on 04/07/14.
//  Copyright (c) 2014 Mayur Pandya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutVC : UIViewController <UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UITableView *tblView;
    
    UIButton *btnRegister;
    UIButton *btnLogin;
    UIButton *btnHey;
}

@end
