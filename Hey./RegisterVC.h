//
//  RegisterVC.h
//  Hey.
//
//  Created by Mayur Pandya on 04/07/14.
//  Copyright (c) 2014 Mayur Pandya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterVC : UIViewController <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    IBOutlet UITableView *tblView;
    
    UITextField *txtMail;
    UITextField *txtUserName;
    UITextField *txtPassword;
    UIButton *btnRegister;
    UIButton *btnGoBack;
    
    
    BOOL keyboardVisible;
    
    UIActivityIndicatorView *actView;
}

@end
