//
//  BlockedUserVC.h
//  Hey.
//
//  Created by Mayur Pandya on 14/07/14.
//  Copyright (c) 2014 Mayur Pandya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BlockedUserVC : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UITableView *tblView;
    
    UIActivityIndicatorView *actView;
    
    int idxPath;
}

@property (nonatomic, strong) NSMutableArray *items;

@end
