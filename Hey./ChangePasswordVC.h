//
//  ChangePasswordVC.h
//  Hey.
//
//  Created by Mayur Pandya on 14/07/14.
//  Copyright (c) 2014 Mayur Pandya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePasswordVC : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIAlertViewDelegate>
{
    IBOutlet UITableView *tblView;
    
    UITextField *txtUserName;
    UITextField *txtOldPassword;
    UITextField *txtNewPassword;
    
    UIButton *btnCP;
    UIButton *btnGoBack;
    
    BOOL keyboardVisible;
    
    UIActivityIndicatorView *actView;
}

@end
