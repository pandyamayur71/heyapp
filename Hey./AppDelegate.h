//
//  AppDelegate.h
//  Hey.
//
//  Created by Mayur Pandya on 04/07/14.
//  Copyright (c) 2014 Mayur Pandya. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MyStoreObserver.h"
#import "MBProgressHUD.h"
#define jsonURL @"http://imobsoftsolutions.in/ws/heyapp/index.php/api/users"

@interface AppDelegate : UIResponder <UIApplicationDelegate,SKProductsRequestDelegate>
{
    NSString *strDeviceToken;
    
    //For In-App purchase
    
    MyStoreObserver *observer;
    
    BOOL isPurchasedOnce, isRestore;
}

@property(nonatomic,strong) MBProgressHUD* activitityIndicator;


@property (strong, nonatomic) UIWindow *window;

@property (nonatomic,retain)UINavigationController *navController;

@property (nonatomic,retain)NSString *strDeviceToken;

//*****
@property (readwrite, nonatomic) BOOL isPurchasedOnce, isRestore;
-(void)requestProductData:(NSString *)p_id;
-(void)restoreInAppPurchase:(NSString *)p_id;
//*****


-(NSMutableArray *)getUserList;
-(NSMutableArray *)getBlockedUserList;

- (void)startActivityIndicator:(UIView *)view;
- (void)stopActivityIndicator;

@end
