//
//  BlockedUserCell.h
//  Hey.
//
//  Created by Mayur Pandya on 14/07/14.
//  Copyright (c) 2014 Mayur Pandya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BlockedUserCell : UITableViewCell
{
    UILabel *lblUser;
    UIButton *btnUnblock;
}

@property (nonatomic,retain)UILabel *lblUser;
@property (nonatomic,retain)UIButton *btnUnblock;

@end
