//
//  SettingsVC.h
//  Hey.
//
//  Created by Mayur Pandya on 14/07/14.
//  Copyright (c) 2014 Mayur Pandya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsVC : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIAlertViewDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate>
{
    
  //  IBOutlet UITableView *tblView;
    
    UILabel *lblUser;
    UILabel *txtUserName;
    
    UIButton *btnHey;
    UIButton *btnUnblock;
    UIButton *btnFeedback;
    UIButton *btnRate;
    UIButton *btnShareApp;
    UIButton *btnPurchaseVoice;
    UIButton *btnLogout;
    
}

@property (strong, nonatomic) IBOutlet UITableView *tblView;

@end
