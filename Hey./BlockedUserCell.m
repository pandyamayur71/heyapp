//
//  BlockedUserCell.m
//  Hey.
//
//  Created by Mayur Pandya on 14/07/14.
//  Copyright (c) 2014 Mayur Pandya. All rights reserved.
//

#import "BlockedUserCell.h"

@implementation BlockedUserCell

@synthesize lblUser,btnUnblock;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        lblUser = [[UILabel alloc] init];
        lblUser.frame = CGRectMake(20, 0, 220, 40);
        lblUser.font = [UIFont boldSystemFontOfSize:20.0];
        lblUser.textAlignment = NSTextAlignmentLeft;
        lblUser.textColor = [UIColor blackColor];
        lblUser.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:lblUser];
        
        btnUnblock = [[UIButton alloc] init];
        [btnUnblock setFrame:CGRectMake(240, 12, 79, 20)];
        [btnUnblock setBackgroundImage:[UIImage imageNamed:@"btnUnblock"] forState:UIControlStateNormal];
        [self.contentView addSubview:btnUnblock];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
