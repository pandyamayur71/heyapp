//
//  ChangePasswordVC.m
//  Hey.
//
//  Created by Mayur Pandya on 14/07/14.
//  Copyright (c) 2014 Mayur Pandya. All rights reserved.
//

#import "ChangePasswordVC.h"

@interface ChangePasswordVC ()

@end

@implementation ChangePasswordVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navBar"] forBarMetrics:UIBarMetricsDefault];
    
    self.title = @"Change Password";
    
    UILabel *nav_titlelbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200,40)];
    nav_titlelbl.text=self.navigationItem.title;;
    nav_titlelbl.textColor=[UIColor whiteColor];
    nav_titlelbl.textAlignment=NSTextAlignmentCenter;
    nav_titlelbl.backgroundColor =[UIColor clearColor];
    nav_titlelbl.adjustsFontSizeToFitWidth=YES;
    UIFont *lblfont=[UIFont systemFontOfSize:25];
    [nav_titlelbl setFont:lblfont];
    self.navigationItem.titleView=nav_titlelbl;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (keyboardDidHide:) name: UIKeyboardDidHideNotification object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    //self.navigationController.navigationBarHidden = TRUE;
    
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.hidesBackButton = YES;
}

#pragma mark -
#pragma mark - TableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    UITextField *textField;
    
    UIButton *btn;
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
    if(indexPath.row == 0)
    {
        txtUserName = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 320, 65)];
        txtUserName.placeholder = @"Username";
        UIColor *color = [UIColor whiteColor];
        txtUserName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Username" attributes:@{NSForegroundColorAttributeName: color}];
        txtUserName.font = [UIFont systemFontOfSize:25.0];
        txtUserName.textColor = [UIColor whiteColor];
        txtUserName.textAlignment = NSTextAlignmentCenter;
        txtUserName.autocorrectionType = UITextAutocorrectionTypeNo;
        
        textField = txtUserName;
        
        [cell.contentView addSubview:textField];
        
        cell.backgroundColor = [self colorWithHexString:@"fcc601"];
        
    }
    if(indexPath.row == 1)
    {
       
        txtOldPassword = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 320, 65)];
        txtOldPassword.placeholder = @"Old Password";
        UIColor *color = [UIColor whiteColor];
        txtOldPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Old Password" attributes:@{NSForegroundColorAttributeName: color}];
        txtOldPassword.font = [UIFont systemFontOfSize:25.0];
        txtOldPassword.textColor = [UIColor whiteColor];
        txtOldPassword.textAlignment = NSTextAlignmentCenter;
        txtOldPassword.secureTextEntry = TRUE;
        
        textField = txtOldPassword;
        
        [cell.contentView addSubview:textField];
        
        cell.backgroundColor = [self colorWithHexString:@"f42f5b"];
        
    }
    if(indexPath.row == 2)
    {
        txtNewPassword = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 320, 65)];
        txtNewPassword.placeholder = @"New Password";
        UIColor *color = [UIColor whiteColor];
        txtNewPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"New Password" attributes:@{NSForegroundColorAttributeName: color}];
        txtNewPassword.font = [UIFont systemFontOfSize:25.0];
        txtNewPassword.textColor = [UIColor whiteColor];
        txtNewPassword.textAlignment = NSTextAlignmentCenter;
        txtNewPassword.secureTextEntry = TRUE;
        
        textField = txtNewPassword;
        
        [cell.contentView addSubview:textField];
        
        cell.backgroundColor = [self colorWithHexString:@"71bc09"];
        
    }
    if(indexPath.row == 3)
    {
        btnCP = [[UIButton alloc] init];
        [btnCP setFrame:CGRectMake(0, 0, 320, 65)];
        [btnCP setTitle:@"Change Password" forState:UIControlStateNormal];
        [btnCP.titleLabel setFont:[UIFont systemFontOfSize:25.0]];
        [btnCP addTarget:self action:@selector(btnChangePwdPClicked) forControlEvents:UIControlEventTouchUpInside];
        
        btn = btnCP;
        
        cell.backgroundColor = [self colorWithHexString:@"e3331c"];
        
    }
    if(indexPath.row == 4)
    {
        btnGoBack = [[UIButton alloc] init];
        [btnGoBack setFrame:CGRectMake(0, 0, 320, 65)];
        [btnGoBack setTitle:@"Go Back" forState:UIControlStateNormal];
        [btnGoBack.titleLabel setFont:[UIFont systemFontOfSize:25.0]];
        [btnGoBack addTarget:self action:@selector(btnGoBackClicked) forControlEvents:UIControlEventTouchUpInside];
        
        btn = btnGoBack;
        
        cell.backgroundColor = [self colorWithHexString:@"0c8fd3"];
    }
    
    textField.delegate = self;
    [cell.contentView addSubview:textField];
    
    [cell.contentView addSubview:btn];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(void)btnChangePwdPClicked
{
    [txtUserName resignFirstResponder];
    [txtOldPassword resignFirstResponder];
    [txtNewPassword resignFirstResponder];
    
    if(![txtUserName.text isEqualToString:@""] & ![txtOldPassword.text isEqualToString:@""] & ![txtNewPassword.text isEqualToString:@""])
    {
        
        //imobsoftsolutions.in/ws/heyapp/index.php/api/users/resetPassword?username=mayur&old_password=j8N745dBV&new_password=mayurtest
        
        self.view.userInteractionEnabled = FALSE;
        
        /*actView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        actView.center = self.view.center;
        actView.color = [self colorWithHexString:@"f4713c"];
        [self.view addSubview:actView];
        [actView startAnimating];*/
        
        [appDelegate startActivityIndicator:appDelegate.window];
        
        
        NSString *postString =[NSString stringWithFormat:@"username=%@&old_password=%@&new_password=%@",txtUserName.text,txtOldPassword.text,txtNewPassword.text];
        
        //NSString *postString =[NSString stringWithFormat:@"device_token=%@&name=Tapan&username=Tapan&password=Tapan&email=a@b.com",strDeviceToken];
        
        //NSLog(@"%@",[postString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]);
        
        NSURL *aUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/resetPassword",jsonURL]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:aUrl
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:60.0];
        
        
        
        
        
        [request setHTTPMethod:@"POST"];
        
        
        NSData *parameterData = [postString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        
        
        
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[parameterData length]];
        
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:parameterData];
        
     //   NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
         {
             //NSLog(@"Response = %@",response.description);
             
             NSString* myString;
             
             myString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
             
             //NSLog(@"Data = %@",myString);
             
             NSDictionary *jsonObject=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
             
             if([[jsonObject valueForKey:@"status"] isEqualToString:@"1"])
             {
                 self.view.userInteractionEnabled = TRUE;
                 //[actView stopAnimating];
                 [appDelegate stopActivityIndicator];
                 
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hey!" message:@"Password has been reset successfully. " delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                 
                 alert.tag = 1;
                 
                 [alert show];
             }
             else
             {
                 self.view.userInteractionEnabled = TRUE;
                 //[actView stopAnimating];
                 [appDelegate stopActivityIndicator];
                 
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hey!" message:@"Please enter valid username or old password." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                 [alert show];
             }
             // return 1;
         }];
        
    }
    
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hey!" message:@"All fields are mendatory." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
        
        alert = nil;
    }
}

-(void)btnGoBackClicked
{
    [txtUserName resignFirstResponder];
    [txtOldPassword resignFirstResponder];
    [txtNewPassword resignFirstResponder];
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark - Alert View Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1)
    {
        if(buttonIndex == 0)
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

#pragma mark -
#pragma mark - Text Field Delegate Methods
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    /*if(textField == txtUserName)
     {
     [tblView setContentOffset:CGPointMake(0,textField.center.y-30) animated:YES];
     }
     
     if(textField == txtPassword)
     {
     [tblView setContentOffset:CGPointMake(0,textField.center.y-30) animated:YES];
     }*/
    
    keyboardVisible = YES;
    
    return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    
    NSInteger nextTag = textField.tag + 1;
    
    UIResponder *nextResponder = [textField.superview viewWithTag:nextTag];
    
    if (nextResponder)
    {
        [tblView setContentOffset:CGPointMake(0,textField.center.y-60) animated:YES];
        
        [nextResponder becomeFirstResponder];
    }
    else
    {
        //[self btnLogin_clicked:Nil];
        [tblView setContentOffset:CGPointMake(0,0) animated:YES];
        [textField resignFirstResponder];
        return YES;
    }
    
    return NO;
    
}

-(void)keyboardDidHide: (NSNotification *)notif
{
	[tblView setContentOffset:CGPointMake(0,0) animated:YES];
    
	keyboardVisible = NO;
	
}

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
