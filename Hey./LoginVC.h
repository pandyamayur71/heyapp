//
//  LoginVC.h
//  Hey.
//
//  Created by Mayur Pandya on 04/07/14.
//  Copyright (c) 2014 Mayur Pandya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginVC : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIAlertViewDelegate>
{
    IBOutlet UITableView *tblView;
    
    UITextField *txtUserName;
    UITextField *txtPassword;
    UIButton *btnLogin;
    UIButton *btnFP;
    UIButton *btnCP;
    UIButton *btnGoBack;
    
    
    BOOL keyboardVisible;
    
    UIActivityIndicatorView *actView;
    
    UITextField  *txtField;
    UITextField *txtField1, *txtField2, *txtField3;
}

@end
