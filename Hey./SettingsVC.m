//
//  SettingsVC.m
//  Hey.
//
//  Created by Mayur Pandya on 14/07/14.
//  Copyright (c) 2014 Mayur Pandya. All rights reserved.
//

#import "SettingsVC.h"
#import <Social/Social.h>
#import "whatIsHeyVC.h"
@interface SettingsVC ()

@end

@implementation SettingsVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    /*[self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                             forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;*/
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navBar"] forBarMetrics:UIBarMetricsDefault];
    
    self.title = @"Settings";
    
    UILabel *nav_titlelbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200,40)];
    nav_titlelbl.text=self.navigationItem.title;;
    nav_titlelbl.textColor=[UIColor whiteColor];
    nav_titlelbl.textAlignment=NSTextAlignmentCenter;
    nav_titlelbl.backgroundColor =[UIColor clearColor];
    nav_titlelbl.adjustsFontSizeToFitWidth=YES;
    UIFont *lblfont=[UIFont systemFontOfSize:25];
    [nav_titlelbl setFont:lblfont];
    self.navigationItem.titleView=nav_titlelbl;
    
    UIImage *menuImage = [UIImage imageNamed:@"btnBack.png"];
    UIButton *face = [UIButton buttonWithType:UIButtonTypeCustom];
    [face addTarget:self action:@selector(btnBack_clicked) forControlEvents:UIControlEventTouchUpInside];
    face.bounds = CGRectMake( 0, 0, menuImage.size.width, menuImage.size.height );
    [face setImage:menuImage forState:UIControlStateNormal];
    UIBarButtonItem *faceBtn = [[UIBarButtonItem alloc] initWithCustomView:face];
    
    self.navigationItem.leftBarButtonItem = faceBtn;
    
    [self.view setBackgroundColor: [self colorWithHexString:@"f2f2f2"]];
}

-(void)btnBack_clicked
{
    [self.navigationController popViewControllerAnimated:YES];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(RemoveAds) name:@"RemoveAds" object:nil];
}
-(void)RemoveAds
{
    [self.tblView reloadData];
}

#pragma mark -
#pragma mark - TableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"RemoveAdsValue"])
    {
        return 8;
    }
    else
    {
        return 7;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"RemoveAdsValue"])
    {
        return 60;
    }
    else
    {
        return 62.3;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    UITextField *textField;
    
    UIButton *btn;
    
    UILabel *lbl;
    UILabel *lbl1;
    
  //  if (cell == nil)
    //{
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
   // }
    
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if (screenSize.height == 480.0f && ![[NSUserDefaults standardUserDefaults] boolForKey:@"RemoveAdsValue"])
        {
            self.tblView.scrollEnabled = YES;
        }
        else
        {
            self.tblView.scrollEnabled = FALSE;
        }
    }
    
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"RemoveAdsValue"])
    {
        if(indexPath.row == 0)
        {
            lblUser = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, 90, 65)];
            lblUser.text =@"USER :";
            lblUser.font = [UIFont systemFontOfSize:25.0];
            lblUser.textColor = [UIColor whiteColor];
            lblUser.textAlignment = NSTextAlignmentLeft;
            
            lbl = lblUser;
            [cell.contentView addSubview:lbl];
            
            txtUserName = [[UILabel alloc] initWithFrame:CGRectMake(95, 0, 260, 65)];
            txtUserName.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
            txtUserName.font = [UIFont systemFontOfSize:30.0];
            txtUserName.textColor = [UIColor whiteColor];
            txtUserName.textAlignment = NSTextAlignmentLeft;
            
            lbl1 = txtUserName;
            
            [cell.contentView addSubview:textField];
            
            cell.backgroundColor = [self colorWithHexString:@"fcc601"];
            
        }
        if(indexPath.row == 1)
        {
            btnHey = [[UIButton alloc] init];
            [btnHey setFrame:CGRectMake(0, 0, 320, 65)];
            [btnHey setTitle:@"WHAT IS HEY APP!" forState:UIControlStateNormal];
            [btnHey.titleLabel setFont:[UIFont systemFontOfSize:25.0]];
            [btnHey addTarget:self action:@selector(btnHey) forControlEvents:UIControlEventTouchUpInside];
            
            btn = btnHey;
            
            cell.backgroundColor = [self colorWithHexString:@"f42f5b"];
            
            
        }
        if(indexPath.row == 2)
        {
            btnUnblock = [[UIButton alloc] init];
            [btnUnblock setFrame:CGRectMake(0, 0, 320, 65)];
            [btnUnblock setTitle:@"BLOCKED USERS" forState:UIControlStateNormal];
            [btnUnblock.titleLabel setFont:[UIFont systemFontOfSize:25.0]];
            [btnUnblock addTarget:self action:@selector(btnUnblock) forControlEvents:UIControlEventTouchUpInside];
            
            btn = btnUnblock;
            
            cell.backgroundColor = [self colorWithHexString:@"71bc09"];
            
        }
        if(indexPath.row == 3)
        {
            btnFeedback = [[UIButton alloc] init];
            [btnFeedback setFrame:CGRectMake(0, 0, 320, 65)];
            [btnFeedback setTitle:@"FEEDBACK" forState:UIControlStateNormal];
            [btnFeedback.titleLabel setFont:[UIFont systemFontOfSize:25.0]];
            [btnFeedback addTarget:self action:@selector(btnFeedback) forControlEvents:UIControlEventTouchUpInside];
            
            btn = btnFeedback;
            
            cell.backgroundColor = [self colorWithHexString:@"e3331c"];
            
            
        }
        if(indexPath.row == 4)
        {
            btnRate = [[UIButton alloc] init];
            [btnRate setFrame:CGRectMake(0, 0, 320, 65)];
            [btnRate setTitle:@"RATE" forState:UIControlStateNormal];
            [btnRate.titleLabel setFont:[UIFont systemFontOfSize:25.0]];
            [btnRate addTarget:self action:@selector(btnRate) forControlEvents:UIControlEventTouchUpInside];
            
            btn = btnRate;
            
            cell.backgroundColor = [self colorWithHexString:@"7d8105"];
        }
        if(indexPath.row == 5)
        {
            btnShareApp = [[UIButton alloc] init];
            [btnShareApp setFrame:CGRectMake(0, 0, 320, 65)];
            [btnShareApp setTitle:@"SHARE APP" forState:UIControlStateNormal];
            [btnShareApp.titleLabel setFont:[UIFont systemFontOfSize:25.0]];
            [btnShareApp addTarget:self action:@selector(btnShareApp) forControlEvents:UIControlEventTouchUpInside];
            
            btn = btnShareApp;
            
            cell.backgroundColor = [self colorWithHexString:@"fcc601"];
            
        }
        
        if(indexPath.row == 6)
        {
            btnPurchaseVoice = [[UIButton alloc] init];
            [btnPurchaseVoice setFrame:CGRectMake(0, 0, 320, 65)];
            [btnPurchaseVoice setTitle:@"PURCHASE VOICES" forState:UIControlStateNormal];
            [btnPurchaseVoice.titleLabel setFont:[UIFont systemFontOfSize:25.0]];
            [btnPurchaseVoice addTarget:self action:@selector(btnPurchaseVoice) forControlEvents:UIControlEventTouchUpInside];
            
            btn = btnPurchaseVoice;
            
            cell.backgroundColor = [self colorWithHexString:@"71bc09"];
            
        }
        
        if(indexPath.row == 7)
        {
            btnLogout = [[UIButton alloc] init];
            [btnLogout setFrame:CGRectMake(0, 0, 320, 65)];
            [btnLogout setTitle:@"LOGOUT" forState:UIControlStateNormal];
            [btnLogout.titleLabel setFont:[UIFont systemFontOfSize:25.0]];
            [btnLogout addTarget:self action:@selector(btnLogout) forControlEvents:UIControlEventTouchUpInside];
            
            btn = btnLogout;
            
            cell.backgroundColor = [self colorWithHexString:@"f42f5b"];
        }

    }
    else
    {
        if(indexPath.row == 0)
        {
            lblUser = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, 90, 65)];
            lblUser.text =@"USER :";
            lblUser.font = [UIFont systemFontOfSize:25.0];
            lblUser.textColor = [UIColor whiteColor];
            lblUser.textAlignment = NSTextAlignmentLeft;
            
            lbl = lblUser;
            [cell.contentView addSubview:lbl];
            
            txtUserName = [[UILabel alloc] initWithFrame:CGRectMake(95, 0, 260, 65)];
            txtUserName.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
            txtUserName.font = [UIFont systemFontOfSize:30.0];
            txtUserName.textColor = [UIColor whiteColor];
            txtUserName.textAlignment = NSTextAlignmentLeft;
            
            lbl1 = txtUserName;
            
            [cell.contentView addSubview:textField];
            
            cell.backgroundColor = [self colorWithHexString:@"fcc601"];
            
        }
        if(indexPath.row == 1)
        {
            btnHey = [[UIButton alloc] init];
            [btnHey setFrame:CGRectMake(0, 0, 320, 65)];
            [btnHey setTitle:@"WHAT IS HEY APP!" forState:UIControlStateNormal];
            [btnHey.titleLabel setFont:[UIFont systemFontOfSize:25.0]];
            [btnHey addTarget:self action:@selector(btnHey) forControlEvents:UIControlEventTouchUpInside];
            
            btn = btnHey;
            
            cell.backgroundColor = [self colorWithHexString:@"f42f5b"];
            
            
        }
        if(indexPath.row == 2)
        {
            btnUnblock = [[UIButton alloc] init];
            [btnUnblock setFrame:CGRectMake(0, 0, 320, 65)];
            [btnUnblock setTitle:@"BLOCKED USERS" forState:UIControlStateNormal];
            [btnUnblock.titleLabel setFont:[UIFont systemFontOfSize:25.0]];
            [btnUnblock addTarget:self action:@selector(btnUnblock) forControlEvents:UIControlEventTouchUpInside];
            
            btn = btnUnblock;
            
            cell.backgroundColor = [self colorWithHexString:@"71bc09"];
            
        }
        if(indexPath.row == 3)
        {
            btnFeedback = [[UIButton alloc] init];
            [btnFeedback setFrame:CGRectMake(0, 0, 320, 65)];
            [btnFeedback setTitle:@"FEEDBACK" forState:UIControlStateNormal];
            [btnFeedback.titleLabel setFont:[UIFont systemFontOfSize:25.0]];
            [btnFeedback addTarget:self action:@selector(btnFeedback) forControlEvents:UIControlEventTouchUpInside];
            
            btn = btnFeedback;
            
            cell.backgroundColor = [self colorWithHexString:@"e3331c"];
            
            
        }
        if(indexPath.row == 4)
        {
            btnRate = [[UIButton alloc] init];
            [btnRate setFrame:CGRectMake(0, 0, 320, 65)];
            [btnRate setTitle:@"RATE" forState:UIControlStateNormal];
            [btnRate.titleLabel setFont:[UIFont systemFontOfSize:25.0]];
            [btnRate addTarget:self action:@selector(btnRate) forControlEvents:UIControlEventTouchUpInside];
            
            btn = btnRate;
            
            cell.backgroundColor = [self colorWithHexString:@"7d8105"];
        }
        if(indexPath.row == 5)
        {
            btnShareApp = [[UIButton alloc] init];
            [btnShareApp setFrame:CGRectMake(0, 0, 320, 65)];
            [btnShareApp setTitle:@"SHARE APP" forState:UIControlStateNormal];
            [btnShareApp.titleLabel setFont:[UIFont systemFontOfSize:25.0]];
            [btnShareApp addTarget:self action:@selector(btnShareApp) forControlEvents:UIControlEventTouchUpInside];
            
            btn = btnShareApp;
            
            cell.backgroundColor = [self colorWithHexString:@"fcc601"];
            
        }
        if(indexPath.row == 6)
        {
            btnLogout = [[UIButton alloc] init];
            [btnLogout setFrame:CGRectMake(0, 0, 320, 65)];
            [btnLogout setTitle:@"LOGOUT" forState:UIControlStateNormal];
            [btnLogout.titleLabel setFont:[UIFont systemFontOfSize:25.0]];
            [btnLogout addTarget:self action:@selector(btnLogout) forControlEvents:UIControlEventTouchUpInside];
            
            btn = btnLogout;
            
            cell.backgroundColor = [self colorWithHexString:@"f42f5b"];
        }
    }
    
    
    [cell.contentView addSubview:lbl];
    [cell.contentView addSubview:lbl1];
    
    textField.delegate = self;
    [cell.contentView addSubview:textField];
    
    [cell.contentView addSubview:btn];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}



-(void)btnHey
{
    whatIsHeyVC *what = [[whatIsHeyVC alloc] initWithNibName:@"whatIsHeyVC" bundle:nil];
    
    [self.navigationController pushViewController:what animated:YES];
}

-(void)btnUnblock
{
    BlockedUserVC *objBUVC = [[BlockedUserVC alloc] initWithNibName:@"BlockedUserVC" bundle:nil];
    
    [self.navigationController pushViewController:objBUVC animated:YES];
}

-(void)btnFeedback
{
    if([MFMailComposeViewController canSendMail])
    {
     
        //mail composer window
        MFMailComposeViewController *emailDialog = [[MFMailComposeViewController alloc] init];
        
        NSArray *toRecipients = [NSArray arrayWithObject:@"info@imobsoftsolutions.in"];
        [emailDialog setToRecipients:toRecipients];
        
        emailDialog.mailComposeDelegate = self;
        [emailDialog setSubject:@"Feedback"];
        
        //[self presentModalViewController:emailDialog animated:YES];
        [self presentViewController:emailDialog animated:YES completion:^(void){}];
        
    }
    else
    {
        UIAlertView *shareAlert = [[UIAlertView alloc]initWithTitle:@"Hey!" message:@"Please configure youe E-Mail Account." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [shareAlert show];
        
        shareAlert = nil;
    }
}

-(void)btnRate
{
    NSString *theUrl = @"itms-apps://itunes.apple.com/app/id899030316";
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:theUrl]];
}

-(void)btnShareApp
{
    UIAlertView *shareAlert = [[UIAlertView alloc]initWithTitle:@"Share App Via:" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Facebook",@"Twitter",@"Mail",@"Message", nil];
    
    shareAlert.tag = 1;
    
    [shareAlert show];
    
    shareAlert = nil;
}

-(void)btnPurchaseVoice
{
    UIAlertView *shareAlert = [[UIAlertView alloc]initWithTitle:@"Hey!" message:@"In-App Purchase:" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Purchase Voices",@"Restore Purchase", nil];
    
    shareAlert.tag = 2;
    
    [shareAlert show];
    
    shareAlert = nil;
}

-(void)btnLogout
{
    AboutVC *objAVC = [[AboutVC alloc] initWithNibName:@"AboutVC" bundle:nil];
    
    [[NSUserDefaults standardUserDefaults] setValue:0 forKey:@"id"];
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"username"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    /*[self.navigationController pushViewController:objLVC animated:NO];
     
     [objLVC release];
     objLVC = nil;*/
    
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    
    transition.type = kCATransitionPush;
    transition.subtype =kCATransitionFromLeft;
    transition.delegate = self;
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController pushViewController:objAVC animated:NO];
    
    objAVC = nil;
    
    
    
}

#pragma mark -
#pragma mark - AlertView Methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1)
    {
        if(buttonIndex == 1)
        {
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
            {
                __block __weak SLComposeViewController *objFB = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
                // todo
                [objFB setInitialText:@"Hey! iPhone App, in that you can send voice notification to your friends."];
                
                [objFB addURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/heeey!/id899030316?ls=1&mt=8"]];
                
                [objFB addImage:[UIImage imageNamed:@"Icon.png"]];
                
                [self presentViewController:objFB animated:YES completion:nil];
                
                [objFB setCompletionHandler:^(SLComposeViewControllerResult result)
                 {
                     NSString *output;
                     
                     if (result == SLComposeViewControllerResultDone)
                     {
                         // NSLog(@"Done successfully");
                         output = @"Psoted successfully in your account.";
                     }
                     else
                     {
                         // NSLog(@"Cancelled");
                         output = @"Status cancelled.";
                         
                     }
                     [self performSelectorOnMainThread:@selector(displayTextFB:)
                                            withObject:output waitUntilDone:NO];
                     
                     [objFB dismissViewControllerAnimated:YES completion:nil];
                     
                 }];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Hey!" message:@"Please set up your Facebook account in settings" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                
                [alert show];
            }
        }
        else if (buttonIndex == 2)
        {
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
            {
                __block __weak SLComposeViewController *objTwit = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                // todo
                [objTwit setInitialText:@"Hey! iPhone App, in that you can send voice notification to your friends."];
                
                [objTwit addURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/heeey!/id899030316?ls=1&mt=8"]];
                [objTwit addImage:[UIImage imageNamed:@"Icon.png"]];
                [self presentViewController:objTwit animated:YES completion:nil];
                
                [objTwit setCompletionHandler:^(SLComposeViewControllerResult result)
                 {
                     NSString *output;
                     
                     if (result == SLComposeViewControllerResultDone)
                     {
                         //NSLog(@"Done successfully");
                         output = @"Tweet done.";
                     }
                     else
                     {
                         // NSLog(@"Cancelled");
                         output = @"Tweet cancelled.";
                     }
                     
                     [self performSelectorOnMainThread:@selector(displayText:) withObject:output waitUntilDone:NO];
                     
                     [objTwit dismissViewControllerAnimated:YES completion:nil];
                     
                 }];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Emotions" message:@"Please set up your Twitter account in settings" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                
                [alert show];
            }
        }
        else if (buttonIndex == 3)
        {
            if([MFMailComposeViewController canSendMail])
            {
                
                MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
                picker.mailComposeDelegate = self;
                
                [picker setSubject:@"Hey! iPhone App."];
                
                NSArray *toRecipients = [NSArray arrayWithObject:@""];
                [picker setToRecipients:toRecipients];
                
               // NSString *emailBody = @"https://itunes.apple.com/us/app/heeey!/id899030316?ls=1&mt=8";
                
                
                NSMutableString *emailBody = [[NSMutableString alloc] initWithString:@"<html><body>"];
                
                NSString *str = @"https://itunes.apple.com/us/app/heeey!/id899030316?ls=1&mt=8";;
                
                [emailBody appendString:[NSString stringWithFormat:@"<b><u><a href=\"%@\">Click here to download application.</a></u></b>", str]];
                
                [picker setMessageBody:emailBody isHTML:YES];
                
                [self presentViewController:picker animated:YES completion:nil];
            }
            else
            {
                UIAlertView *shareAlert = [[UIAlertView alloc]initWithTitle:@"Hey!" message:@"Please configure youe E-Mail Account." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                
                [shareAlert show];
                
                shareAlert = nil;
            }
        }
        else if (buttonIndex == 4)
        {
            MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
            if([MFMessageComposeViewController canSendText])
            {
                controller.body = @"https://itunes.apple.com/us/app/heeey!/id899030316?ls=1&mt=8";
                controller.recipients = nil;
                controller.messageComposeDelegate = self;
                [self presentViewController:controller animated:YES completion:nil];
            }
        }
    }
    if(alertView.tag == 2)
    {
        if(buttonIndex == 1)
        {
            [appDelegate requestProductData:@"HeyVoices"];
        }
        
        if(buttonIndex == 2)
        {
            [appDelegate restoreInAppPurchase:@"HeyVoices"];
        }
    }
}

- (void)displayTextFB:(NSString *)text
{
    if([text isEqualToString:@"Posted successfully in your account."])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Sucsess" message:text delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Failed" message:text delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)displayText:(NSString *)text
{
    if([text isEqualToString:@"Tweet done."])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Sucsess" message:text delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Failed" message:text delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark -
#pragma mark - Message Methods

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
	switch (result)
    {
		case MessageComposeResultCancelled:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Cancel" message:@"SMS has been cancelled... !!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alert show];
            alert = nil;
            
            break;
            
        }
            
		case MessageComposeResultFailed:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failed" message:@"SMS has been Failed... !!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alert show];
            alert = nil;
            break;
        }
		case MessageComposeResultSent:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success" message:@"SMS has been sent successfully... !!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            alert = nil;
            break;
        }
		default:
			break;
	}
    
	[self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark - Email Methods

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MFMailComposeResultCancelled:
            NSLog(@"Result: Mail sending canceled");
            self.navigationItem.leftBarButtonItem.enabled = TRUE;
			break;
            
		case MFMailComposeResultSaved:
			NSLog(@"Result: Mail saved");
			break;
            
		case MFMailComposeResultSent:
			NSLog(@"Result: Mail sent");
			break;
            
		case MFMailComposeResultFailed:
			NSLog(@"Result: Mail sending failed");
			break;
            
		default:
			NSLog(@"Result: Mail not sent");
			break;
	}
    
    [self dismissViewControllerAnimated:YES completion:^(void){}];
}

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
