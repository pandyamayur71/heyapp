//
//  AboutVC.m
//  Hey.
//
//  Created by Mayur Pandya on 04/07/14.
//  Copyright (c) 2014 Mayur Pandya. All rights reserved.
//

#import "AboutVC.h"
#import "whatIsHeyVC.h"
//#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

@interface AboutVC ()

@end

@implementation AboutVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        //commit.... it
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    
    /*[self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;*/
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navBar"] forBarMetrics:UIBarMetricsDefault];
    
    self.title = @"Hey!";
    
    UILabel *nav_titlelbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200,40)];
    nav_titlelbl.text=self.navigationItem.title;;
    nav_titlelbl.textColor=[UIColor whiteColor];
    nav_titlelbl.textAlignment=NSTextAlignmentCenter;
    nav_titlelbl.backgroundColor =[UIColor clearColor];
    nav_titlelbl.adjustsFontSizeToFitWidth=YES;
    UIFont *lblfont=[UIFont systemFontOfSize:25];
    [nav_titlelbl setFont:lblfont];
    self.navigationItem.titleView=nav_titlelbl;
    
    
    [self.view setBackgroundColor: [self colorWithHexString:@"f2f2f2"]];
    
   /* int fromNumber = 10;
    int toNumber = 30;
    int randomNumber = (arc4random()%(toNumber-fromNumber))+fromNumber;
    
    NSLog(@"%d",randomNumber);*/
    
    //Mayur Commit
}

-(void)viewWillAppear:(BOOL)animated
{
   // self.navigationController.navigationBarHidden = TRUE;
    
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.hidesBackButton = YES;
    
}

#pragma mark -
#pragma mark - TableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    UIButton *btn;
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if(indexPath.row == 0)
    {
        btnRegister = [[UIButton alloc] init];
        [btnRegister setFrame:CGRectMake(0, 0, 320, 65)];
        [btnRegister setTitle:@"Register" forState:UIControlStateNormal];
        [btnRegister.titleLabel setFont:[UIFont systemFontOfSize:25.0]];
        [btnRegister addTarget:self action:@selector(btnRegisterClicked) forControlEvents:UIControlEventTouchUpInside];
        
        btn = btnRegister;
        
        cell.backgroundColor = [self colorWithHexString:@"fcc601"];
    }
    if(indexPath.row == 1)
    {
        btnLogin = [[UIButton alloc] init];
        [btnLogin setFrame:CGRectMake(0, 0, 320, 65)];
        [btnLogin setTitle:@"Login" forState:UIControlStateNormal];
        [btnLogin.titleLabel setFont:[UIFont systemFontOfSize:25.0]];
        [btnLogin addTarget:self action:@selector(btnLoginClicked) forControlEvents:UIControlEventTouchUpInside];
        
        btn = btnLogin;
        
        cell.backgroundColor = [self colorWithHexString:@"f42f5b"];
    }
    if(indexPath.row == 2)
    {
        btnHey = [[UIButton alloc] init];
        [btnHey setFrame:CGRectMake(0, 0, 320, 65)];
        [btnHey setTitle:@"What is Hey App?" forState:UIControlStateNormal];
        [btnHey.titleLabel setFont:[UIFont systemFontOfSize:25.0]];
        [btnHey addTarget:self action:@selector(btnHeyClicked) forControlEvents:UIControlEventTouchUpInside];
        
        btn = btnHey;
        
        cell.backgroundColor = [self colorWithHexString:@"71bc09"];
    }
    
    [cell.contentView addSubview:btn];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

-(void)btnRegisterClicked
{
    RegisterVC *objRVC = [[RegisterVC alloc] initWithNibName:@"RegisterVC" bundle:nil];
    
    [self.navigationController pushViewController:objRVC animated:YES];
}

-(void)btnLoginClicked
{
    LoginVC *objLVC = [[LoginVC alloc] initWithNibName:@"LoginVC" bundle:nil];
    [self.navigationController pushViewController:objLVC animated:YES];
}

-(void)btnHeyClicked
{
    whatIsHeyVC *what = [[whatIsHeyVC alloc] initWithNibName:@"whatIsHeyVC" bundle:nil];
    
    [self.navigationController pushViewController:what animated:YES];

}

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
