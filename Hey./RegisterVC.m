//
//  RegisterVC.m
//  Hey.
//
//  Created by Mayur Pandya on 04/07/14.
//  Copyright (c) 2014 Mayur Pandya. All rights reserved.
//

#import "RegisterVC.h"

@interface RegisterVC ()

@end

@implementation RegisterVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navBar"] forBarMetrics:UIBarMetricsDefault];
    
    self.title = @"Register";
    
    UILabel *nav_titlelbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200,40)];
    nav_titlelbl.text=self.navigationItem.title;;
    nav_titlelbl.textColor=[UIColor whiteColor];
    nav_titlelbl.textAlignment=NSTextAlignmentCenter;
    nav_titlelbl.backgroundColor =[UIColor clearColor];
    nav_titlelbl.adjustsFontSizeToFitWidth=YES;
    UIFont *lblfont=[UIFont systemFontOfSize:25];
    [nav_titlelbl setFont:lblfont];
    self.navigationItem.titleView=nav_titlelbl;
    
    [self.view setBackgroundColor: [self colorWithHexString:@"f2f2f2"]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (keyboardDidHide:) name: UIKeyboardDidHideNotification object:nil];
    
    
    
   // NSLog(@"%@",tblView);
}

-(void)viewWillAppear:(BOOL)animated
{
    //self.navigationController.navigationBarHidden = TRUE;
    
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.hidesBackButton = YES;
}

#pragma mark -
#pragma mark - TableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    UITextField *textField;
    
    UIButton *btn;
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if(indexPath.row == 0)
    {
        txtMail = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 320, 65)];
        txtMail.placeholder = @"E-Mail";
        UIColor *color = [UIColor whiteColor];
        txtMail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"E-Mail" attributes:@{NSForegroundColorAttributeName: color}];
        txtMail.font = [UIFont systemFontOfSize:25.0];
        txtMail.textColor = [UIColor whiteColor];
        txtMail.textAlignment = NSTextAlignmentCenter;
        txtMail.autocorrectionType = UITextAutocorrectionTypeNo;
        txtMail.keyboardType = UIKeyboardTypeEmailAddress;
        
        textField = txtMail;
        
        [cell.contentView addSubview:textField];
        
        cell.backgroundColor = [self colorWithHexString:@"fcc601"];
    }
    if(indexPath.row == 1)
    {
        txtUserName = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 320, 65)];
        txtUserName.placeholder = @"Username";
        UIColor *color = [UIColor whiteColor];
        txtUserName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Username" attributes:@{NSForegroundColorAttributeName: color}];
        txtUserName.font = [UIFont systemFontOfSize:25.0];
        txtUserName.textColor = [UIColor whiteColor];
        txtUserName.textAlignment = NSTextAlignmentCenter;
        txtUserName.autocorrectionType = UITextAutocorrectionTypeNo;
        
        textField = txtUserName;
        
        [cell.contentView addSubview:textField];
        
        cell.backgroundColor = [self colorWithHexString:@"f42f5b"];
    }
    if(indexPath.row == 2)
    {
        txtPassword = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 320, 65)];
        txtPassword.placeholder = @"Password";
        UIColor *color = [UIColor whiteColor];
        txtPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];
        txtPassword.font = [UIFont systemFontOfSize:25.0];
        txtPassword.textColor = [UIColor whiteColor];
        txtPassword.textAlignment = NSTextAlignmentCenter;
        txtPassword.secureTextEntry = TRUE;
        [txtPassword setSecureTextEntry:YES];
        textField = txtPassword;
        
        [cell.contentView addSubview:textField];
        
        cell.backgroundColor = [self colorWithHexString:@"71bc09"];
    }
    if(indexPath.row == 3)
    {
        btnRegister = [[UIButton alloc] init];
        [btnRegister setFrame:CGRectMake(0, 0, 320, 65)];
        [btnRegister setTitle:@"Register" forState:UIControlStateNormal];
        [btnRegister.titleLabel setFont:[UIFont systemFontOfSize:25.0]];
        [btnRegister addTarget:self action:@selector(btnRegisterClicked) forControlEvents:UIControlEventTouchUpInside];
        
        btn = btnRegister;
        
        cell.backgroundColor = [self colorWithHexString:@"e3331c"];
    }
    if(indexPath.row == 4)
    {
        btnGoBack = [[UIButton alloc] init];
        [btnGoBack setFrame:CGRectMake(0, 0, 320, 65)];
        [btnGoBack setTitle:@"Go Back" forState:UIControlStateNormal];
        [btnGoBack.titleLabel setFont:[UIFont systemFontOfSize:25.0]];
        [btnGoBack addTarget:self action:@selector(btnGoBackClicked) forControlEvents:UIControlEventTouchUpInside];
        
        btn = btnGoBack;
        
        cell.backgroundColor = [self colorWithHexString:@"0c8fd3"];
    }
    
   
    
    textField.delegate = self;
    [cell.contentView addSubview:textField];
    
    [cell.contentView addSubview:btn];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark -
#pragma mark - Custom Methods

-(void)btnRegisterClicked
{
    [txtMail resignFirstResponder];
    [txtUserName resignFirstResponder];
    [txtPassword resignFirstResponder];
    
    if([self isEmailValid:txtMail.text] & ![txtUserName.text isEqualToString:@""] & ![txtPassword.text isEqualToString:@""])
    {
//       __block int retVal = [appDelegate ws_RegisterUser:[NSMutableDictionary dictionaryWithObjectsAndKeys:txtMail.text, @"email", txtUserName.text, @"username",txtUserName.text, @"name",  txtPassword.text, @"password", nil]];
//        
//        if(retVal == 1)
//        {
//            // [self.navigationController popViewControllerAnimated:YES];
//            UsersVC *objUVC = [[UsersVC alloc] initWithNibName:@"UsersVC" bundle:nil];
//            [self.navigationController pushViewController:objUVC animated:YES];
//        }
        
        self.view.userInteractionEnabled = FALSE;
        
     
        [appDelegate startActivityIndicator:appDelegate.window];
        
        appDelegate.strDeviceToken = @"43a41bb1b52763eb1daec62add0021f6e4c3a0371a3fa79a108ce1de563e8c08";
        
        NSString *postString =[NSString stringWithFormat:@"device_token=%@&device_type=iphone&name=%@&username=%@&password=%@&email=%@",appDelegate.strDeviceToken,txtUserName.text,txtUserName.text,txtPassword.text,txtMail.text];
        
        //NSString *postString =[NSString stringWithFormat:@"device_token=%@&name=Tapan&username=Tapan&password=Tapan&email=a@b.com",strDeviceToken];
        
       // NSLog(@"%@",[postString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]);
        
        NSURL *aUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/register",jsonURL]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:aUrl
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:60.0];
        
        
        
        
        
        [request setHTTPMethod:@"POST"];
        
        
        NSData *parameterData = [postString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        
        
        
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[parameterData length]];
        
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:parameterData];
        
     //   NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
         {
            // NSLog(@"Response = %@",response.description);
             
             
             [appDelegate stopActivityIndicator];
             
             NSString* myString;
             
             myString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
             
            // NSLog(@"Data = %@",myString);
             
             NSDictionary *jsonObject=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
             
             if([[jsonObject valueForKey:@"status"] isEqualToString:@"1"])
             {
                 self.view.userInteractionEnabled = TRUE;
                 
                 [[NSUserDefaults standardUserDefaults] setValue:[[jsonObject valueForKey:@"user_detail"] valueForKey:@"id"] forKey:@"id"];
                 [[NSUserDefaults standardUserDefaults] setValue:[[jsonObject valueForKey:@"user_detail"] valueForKey:@"username"] forKey:@"username"];
                 [[NSUserDefaults standardUserDefaults] synchronize];
                 
                 
                 
                 
             //    dispatch_sync(dispatch_get_main_queue(),
             //                  ^{
                                   UsersVC *objUVC = [[UsersVC alloc] initWithNibName:@"UsersVC" bundle:nil];
                                   [self.navigationController pushViewController:objUVC animated:YES];
                                   
                                   
                
                //               });//end

                 
                // NSLog(@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"id"]);
                //NSLog(@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"username"]);
             }
             else
             {
                 self.view.userInteractionEnabled = TRUE;
                 [actView stopAnimating];
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hey!" message:@"User Already exists with username or email or device." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                 [alert show];
             }
             // return 1;
         }];
        
    }
    
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hey!" message:@"All fields are mendatory." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
        
        alert = nil;
    }
}

-(void)btnGoBackClicked
{
    [txtMail resignFirstResponder];
    [txtUserName resignFirstResponder];
    [txtPassword resignFirstResponder];
    
   // [self.navigationController popViewControllerAnimated:YES];
    
//    AboutVC *objAVC = [[AboutVC alloc] initWithNibName:@"AboutVC" bundle:nil];
//    
//    CATransition *transition = [CATransition animation];
//    transition.duration = 0.3;
//    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//    
//    transition.type = kCATransitionPush;
//    transition.subtype =kCATransitionFromLeft;
//    transition.delegate = self;
//    [self.navigationController.view.layer addAnimation:transition forKey:nil];
//    
//    self.navigationController.navigationBarHidden = NO;
//    [self.navigationController pushViewController:objAVC animated:NO];
//    
//    objAVC = nil;
    
    [self.navigationController popViewControllerAnimated:YES];
}


-(BOOL)isEmailValid:(NSString*)string
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    if([emailTest evaluateWithObject:string])
        return YES;
    else
        return NO;
}



#pragma mark -
#pragma mark - Text Field Delegate Methods
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    /*if(textField == txtUserName)
    {
        [tblView setContentOffset:CGPointMake(0,textField.center.y-30) animated:YES];
    }
    
    if(textField == txtPassword)
    {
        [tblView setContentOffset:CGPointMake(0,textField.center.y-30) animated:YES];
    }*/
    
    keyboardVisible = YES;
    
    return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    
    NSInteger nextTag = textField.tag + 1;
    
    UIResponder *nextResponder = [textField.superview viewWithTag:nextTag];
    
    if (nextResponder)
    {
        [tblView setContentOffset:CGPointMake(0,textField.center.y-60) animated:YES];
        
        [nextResponder becomeFirstResponder];
    }
    else
    {
        //[self btnLogin_clicked:Nil];
        //[tblView setContentOffset:CGPointMake(0,0) animated:YES];
        [textField resignFirstResponder];
        return YES;
    }
    
    return NO;
    
}

-(void)keyboardDidHide: (NSNotification *)notif
{
	//[tblView setContentOffset:CGPointMake(0,0) animated:YES];
    
	keyboardVisible = NO;
	
}

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
