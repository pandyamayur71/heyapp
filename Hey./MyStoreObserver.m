//
//  MyStoreObserver.m
//  PurchaseSample
//
//  Created by Joey Sacchini on 7/25/09.
//  Copyright 2009 Joey Sacchini. All rights reserved.
//

#import "MyStoreObserver.h"


@implementation MyStoreObserver
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction *transaction in transactions)
    {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
                
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
                
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
                break;
                
            default:
                break;
        }
    }
}

- (void) completeTransaction: (SKPaymentTransaction *)transaction
{
    if(appDelegate.isPurchasedOnce == NO)
    {
        NSLog(@"Complete Transaction For : %@",transaction.description);
        
        [self recordTransaction: transaction];
        
        [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"RemoveAdsValue"];

        [[NSUserDefaults standardUserDefaults]synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RemoveAds" object:nil];
        
        appDelegate.isPurchasedOnce = YES;
        
        //[[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"RemoveAdsValue"];
        
        
        //    [[SKPaymentQueue defaultQueue] ]
        //
        UIAlertView *alertPurchased = [[UIAlertView alloc]initWithTitle:@"Hey!" message:@"Unlocked Extra Voices" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        alertPurchased.tag = 1;
        
        [alertPurchased show];
        alertPurchased = nil;
    }
}

- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    NSLog(@"%@",queue );
    
    NSLog(@"Restored Transactions are once again in Queue for purchasing %@",[queue transactions]);  
    
    NSMutableArray *purchasedItemIDs = [[NSMutableArray alloc] init];
    
    NSLog(@"received restored transactions: %i", queue.transactions.count);
    
    for (SKPaymentTransaction *transaction in queue.transactions)
    {
        NSString *productID = transaction.payment.productIdentifier;
        [purchasedItemIDs addObject:productID];
    }  
}

/*-(void) restoreTransaction: (SKPaymentTransaction *)transaction
{
    [self recordTransaction: transaction];
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"unblockitems" object:nil];
    
    UIAlertView *alertPurchased=[[UIAlertView alloc]initWithTitle:@"iFunbox" message:@"Transactios Restored Successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    alertPurchased.tag = 1;
    
    [alertPurchased show];
    [alertPurchased release];
    alertPurchased = nil;
}
*/
-(void) restoreTransaction: (SKPaymentTransaction *)transaction
{
    if(appDelegate.isPurchasedOnce == NO)
    {
        
        [self recordTransaction: transaction];
        
        [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"RemoveAdsValue"];
        
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RemoveAds" object:nil];
        
        appDelegate.isPurchasedOnce = YES;
        
        UIAlertView *alertPurchased=[[UIAlertView alloc]initWithTitle:@"Hey!" message:@"Transactios Restored Successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        alertPurchased.tag = 1;
        
        [alertPurchased show];
        alertPurchased = nil;
        
    }
}



- (void) failedTransaction: (SKPaymentTransaction *)transaction
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"purchasefailed" object:nil];
    
    if (transaction.error.code != SKErrorPaymentCancelled)
    {
		NSLog(@"Transaction Error: %@", transaction.error.description);
		UIAlertView *alertFailure=[[UIAlertView alloc]initWithTitle:@"Hey!" message:@"Transaction Failed" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
		[alertFailure show];
        // Optionally, display an error here.
    }

	[[NSNotificationCenter defaultCenter] postNotificationName:@"stopActivity" object:nil];
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}
- (void)recordTransaction:(SKPaymentTransaction *)transaction
{
	//Record the purchase here
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
}
@end
