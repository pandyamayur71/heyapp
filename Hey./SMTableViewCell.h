//
//  SMTableViewCell.h
//  SeeMoreCell
//
//  Created by Anoop Mohandas on 23/03/13.
//  Copyright (c) 2013 layraa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMTableViewCell : UITableViewCell
{
    IBOutlet UIButton *btnHeyM;
    IBOutlet UIButton *btnGdMornM;
    IBOutlet UIButton *btnGnM;
    IBOutlet UIButton *btnLoveM;
    IBOutlet UIButton *btnGBM;
    
    IBOutlet UIButton *btnHeyW;
    IBOutlet UIButton *btnGdMornW;
    IBOutlet UIButton *btnGnW;
    IBOutlet UIButton *btnLoveW;
    IBOutlet UIButton *btnGBW;
    
    IBOutlet UIButton *btnBlockUser;
    IBOutlet UIButton *btnDeleteUser;
    
    IBOutlet UIButton *btnMan;
    IBOutlet UIButton *btnWoman;
    
    IBOutlet UIImageView *imgLine1;
    IBOutlet UIImageView *imgLine2;
    IBOutlet UIImageView *imgLine3;
    
    IBOutlet UIView *bgView;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *expandHeightConstant;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalSepLine;

@property (strong, nonatomic) IBOutlet UIImageView *imgViewSepLine;

@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIButton *seeMoreButton;


@property (retain, nonatomic) UIButton *btnHeyM;
@property (retain, nonatomic) UIButton *btnGdMornM;
@property (retain, nonatomic) UIButton *btnGnM;
@property (retain, nonatomic) UIButton *btnLoveM;
@property (retain, nonatomic) UIButton *btnGBM;

@property (retain, nonatomic) UIButton *btnHeyW;
@property (retain, nonatomic) UIButton *btnGdMornW;
@property (retain, nonatomic) UIButton *btnGnW;
@property (retain, nonatomic) UIButton *btnLoveW;
@property (retain, nonatomic) UIButton *btnGBW;

@property (retain, nonatomic) UIButton *btnBlockUser;
@property (retain, nonatomic) UIButton *btnDeleteUser;

@property (retain, nonatomic) UIButton *btnMan;
@property (retain, nonatomic) UIButton *btnWoman;

@property (retain, nonatomic) UIImageView *imgLine1;
@property (retain, nonatomic) UIImageView *imgLine2;
@property (retain, nonatomic) UIImageView *imgLine3;

@property (retain, nonatomic) UIView *bgView;

@end
