//
//  MyStoreObserver.h
//  PurchaseSample
//
//  Created by Joey Sacchini on 7/25/09.
//  Copyright 2009 Joey Sacchini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

@class AppDelegate;

@interface MyStoreObserver : NSObject <SKPaymentTransactionObserver>
{

}

- (void) completeTransaction: (SKPaymentTransaction *)transaction;
- (void) restoreTransaction: (SKPaymentTransaction *)transaction;
- (void) failedTransaction: (SKPaymentTransaction *)transaction;
- (void)recordTransaction:(SKPaymentTransaction *)transaction;


@end
