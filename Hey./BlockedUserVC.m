//
//  BlockedUserVC.m
//  Hey.
//
//  Created by Mayur Pandya on 14/07/14.
//  Copyright (c) 2014 Mayur Pandya. All rights reserved.
//

#import "BlockedUserVC.h"

@interface BlockedUserVC ()

@end

@implementation BlockedUserVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navBar"] forBarMetrics:UIBarMetricsDefault];
    
    self.items = [NSMutableArray new];
    
    self.title = @"Blocked Users";
    
    UILabel *nav_titlelbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200,40)];
    nav_titlelbl.text=self.navigationItem.title;;
    nav_titlelbl.textColor=[UIColor whiteColor];
    nav_titlelbl.textAlignment=NSTextAlignmentCenter;
    nav_titlelbl.backgroundColor =[UIColor clearColor];
    nav_titlelbl.adjustsFontSizeToFitWidth=YES;
    UIFont *lblfont=[UIFont systemFontOfSize:25];
    [nav_titlelbl setFont:lblfont];
    self.navigationItem.titleView=nav_titlelbl;
    
    UIImage *menuImage = [UIImage imageNamed:@"btnBack.png"];
    UIButton *face = [UIButton buttonWithType:UIButtonTypeCustom];
    [face addTarget:self action:@selector(btnBack_clicked) forControlEvents:UIControlEventTouchUpInside];
    face.bounds = CGRectMake( 0, 0, menuImage.size.width, menuImage.size.height );
    [face setImage:menuImage forState:UIControlStateNormal];
    UIBarButtonItem *faceBtn = [[UIBarButtonItem alloc] initWithCustomView:face];
    
    self.navigationItem.leftBarButtonItem = faceBtn;
    
    [self.view setBackgroundColor: [self colorWithHexString:@"f2f2f2"]];
}

-(void)btnBack_clicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = FALSE;
    
    //[self loadData];
    
    
    [appDelegate startActivityIndicator:appDelegate.window];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.view.userInteractionEnabled = FALSE;
        
        [self loadData];
    });
}

-(void)loadData
{
    if([self.items count]>0)
        [self.items removeAllObjects];
    
    NSMutableArray *temp = [appDelegate getBlockedUserList];  //ws_getCategories
    
    [self.items addObjectsFromArray:temp];
    
    [tblView reloadData];
    
    self.view.userInteractionEnabled = TRUE;
    
    [appDelegate stopActivityIndicator];
}

#pragma mark -
#pragma mark - Table View Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  [self.items count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
//{
//    return 65;
//}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    BlockedUserCell *cell = (BlockedUserCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[BlockedUserCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    cell.lblUser.text = [[self.items objectAtIndex:indexPath.row] valueForKey:@"username"];
    cell.lblUser.textColor = [self colorWithHexString:@"f4713c"];
    
    [cell.btnUnblock addTarget:self action:@selector(btnUnblockClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnUnblock setTag:indexPath.row];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(void)btnUnblockClicked:(id)sender
{
    idxPath = [sender tag];
    
    self.view.userInteractionEnabled = FALSE;
    
    /*actView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    actView.center = self.view.center;
   // actView.backgroundColor = [self colorWithHexString:@"f4713c"];
    actView.color = [self colorWithHexString:@"f4713c"];
    [self.view addSubview:actView];
    [actView startAnimating];*/
    
    [appDelegate startActivityIndicator:appDelegate.window];
    
    NSString *postString =[NSString stringWithFormat:@"username=%@&invity_username=%@&blocked=0",[[NSUserDefaults standardUserDefaults] valueForKey:@"username"],[[self.items objectAtIndex:idxPath] valueForKey:@"username"]];
    
    //NSLog(@"%@",[postString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]);
    
    NSURL *aUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/blockUnblock",jsonURL]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:aUrl cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    
    
    
    
    [request setHTTPMethod:@"POST"];
    
    
    NSData *parameterData = [postString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    
    
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[parameterData length]];
    
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:parameterData];
    
  //  NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
         //NSLog(@"Response = %@",response.description);
         
         NSString* myString;
         
         myString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
         
        // NSLog(@"Data = %@",myString);
         
         NSDictionary *jsonObject=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
         
         if([[jsonObject valueForKey:@"status"] isEqualToString:@"1"])
         {
             self.view.userInteractionEnabled = TRUE;
             //[actView stopAnimating];
             [appDelegate stopActivityIndicator];
             
             
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hey!" message:[NSString stringWithFormat:@"%@ has been unblocked successfully.",[[self.items objectAtIndex:idxPath] valueForKey:@"username"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
             
             [alert show];
             
             alert = nil;
             
             [self.items removeObjectAtIndex:idxPath];
             [tblView reloadData];
             //[self loadData];
             
             
             
             
         }
         else
         {
             self.view.userInteractionEnabled = TRUE;
             //[actView stopAnimating];
             [appDelegate stopActivityIndicator];
             
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hey!" message:@"Invalid username. Try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
         }
         
     }];
}

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
