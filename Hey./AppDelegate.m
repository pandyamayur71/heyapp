//
//  AppDelegate.m
//  Hey.
//
//  Created by Mayur Pandya on 04/07/14.
//  Copyright (c) 2014 Mayur Pandya. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate


@synthesize navController;
@synthesize strDeviceToken;
@synthesize activitityIndicator;
@synthesize isPurchasedOnce, isRestore;

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
   // NSLog(@"productsRequest");
    
	NSArray *invalidProudcts = response.invalidProductIdentifiers;
    
	if ([invalidProudcts count] != 0)
	{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"purchasefailed" object:nil];
        
		//NSLog(@"Products Invalid");
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invalid!!" message:@"Need itunesconnect product id.." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
        
        alert = nil;
	}
	
	else
	{
		//NSLog(@"Products Valid");
        
		NSArray *array = [response products];
        
        if([array count] == 0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Unable to retrieve product details from server. Please try again later." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alert show];
            
            alert = nil;
            
            return;
        }
        
		SKProduct *product = [array objectAtIndex:0];
        
		NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
		[numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
		[numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
		[numberFormatter setLocale:product.priceLocale];
        
		NSString *stringPrice = [numberFormatter stringFromNumber:product.price];
		//[dicGoalvarialbs setValue:[NSString stringWithFormat:@"%@",stringPrice] forKey:@"Price"];
		//NSLog(@"Price %@",stringPrice);
        
        observer = [[MyStoreObserver alloc] init];
        [[SKPaymentQueue defaultQueue] addTransactionObserver:observer];
        
        if(isRestore == NO)
        {
            isPurchasedOnce = NO;
            SKPayment *payment = [SKPayment paymentWithProduct:product];
            [[SKPaymentQueue defaultQueue] addPayment:payment];
        }
        
        else
        {
            [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
        }
	}
}

-(void)restoreInAppPurchase:(NSString *)p_id;
{
    //NSLog(@"restoreInAppPurchase");
    
    isRestore = TRUE;
    
    SKProductsRequest *request = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObject:p_id]]; //*Identifer you set up in iTunes Connect* is what the name is.
	//SKProductsRzequest *request= [[SKProductsRequest alloc] initWithProductIdentifiers: [NSSet setWithObject: kMyFeatureIdentifier]];
	request.delegate = self;
	[request start];
}


-(void)requestProductData:(NSString *)p_id
{
    //NSLog(@"requestProductData");
    
    isRestore = FALSE;
    
	SKProductsRequest *request = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObject:p_id]]; //*Identifer you set up in iTunes Connect* is what the name is.
	//SKProductsRzequest *request= [[SKProductsRequest alloc] initWithProductIdentifiers: [NSSet setWithObject: kMyFeatureIdentifier]];
	request.delegate = self;
	[request start];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    
   /* [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
     (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    
    */
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication]
         registerUserNotificationSettings:[UIUserNotificationSettings
                                           settingsForTypes:(UIUserNotificationTypeSound |
                                                             UIUserNotificationTypeAlert | UIUserNotificationTypeBadge)
                                           categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication]
         registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge |
          UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }

    
  //  [[UIApplication sharedApplication]registerUserNotificationSettings:(UIUserNotificationSettings *)];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    
    // NSLog(@"%d",[[[NSUserDefaults standardUserDefaults] valueForKey:@"id"]intValue]);
    
    if(![[NSUserDefaults standardUserDefaults] valueForKey:@"id"])
    {
        AboutVC *objAVC = [[AboutVC alloc] initWithNibName:@"AboutVC" bundle:nil];
        
        self.navController = [[UINavigationController alloc] initWithRootViewController:objAVC];
    }
    
    else
    {
        UsersVC *objUVC = [[UsersVC alloc] initWithNibName:@"UsersVC" bundle:nil];
        
        self.navController = [[UINavigationController alloc] initWithRootViewController:objUVC];
    }
    
    
    
    self.window.rootViewController = self.navController;
    
    [NSThread sleepForTimeInterval:3];
    
   // self.navController.navigationBarHidden = TRUE;
    
   // [[UIApplication sharedApplication] setStatusBarHidden:YES];
  //  [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    [self.window makeKeyAndVisible];
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    // Example URL: myapp://myapp.com/showString/yourString
    
    BOOL isMyAppURL = [[url scheme] isEqualToString:@"hey"];
    //NSLog(@"url recieved: %@", url);
    //NSLog(@"scheme: %@", [url scheme]);
    //NSLog(@"query string: %@", [url query]);
    //NSLog(@"host: %@", [url host]);
    if (isMyAppURL)
    {
     // Invite username webservice will be called from here...
    // username= [ our default username / login username] &invity_username=[url host],
        
        
        NSString *login = [[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
        
        if([login isEqualToString:@""])
        {
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"ALERT !" message:@"Please login into Heyapp, and try again !" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [alert show];
            return isMyAppURL;
            
        }
        else
        {
            [[NSNotificationCenter defaultCenter]postNotificationName:@"addUser" object:[url host]];

        }
        
        
     //   [self addUser:[url host]];
        
        
        
    }
    
    return isMyAppURL;
}

- (void)addUser:(NSString *)invite_username
{
    NSString *login = [[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
    
    if([login isEqualToString:@""])
    {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"ALERT !" message:@"Please login into Heyapp, and try again !" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
        return;
        
    }
    else
    {
        
        [appDelegate.window setUserInteractionEnabled:FALSE];
        
       UIActivityIndicatorView *actView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        actView.center = self.window.center;
        actView.color = [self colorWithHexString:@"f4713c"];
        [self.window addSubview:actView];
        [actView startAnimating];
        
        NSString *postString =[NSString stringWithFormat:@"username=%@&invity_username=%@",login,invite_username];
        
        //NSLog(@"%@",[postString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]);
        
        NSURL *aUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/InviteUser",jsonURL]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:aUrl cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
        
        
        
        
        
        [request setHTTPMethod:@"POST"];
        
        
        NSData *parameterData = [postString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        
        
        
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[parameterData length]];
        
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:parameterData];
        
       // NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
         {
             //NSLog(@"Response = %@",response.description);
             
             NSString* myString;
             
             myString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
             
             //NSLog(@"Data = %@",myString);
             
             NSDictionary *jsonObject=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
             
             if([[jsonObject valueForKey:@"status"] isEqualToString:@"1"])
             {
                 self.window.userInteractionEnabled = TRUE;
                 [actView stopAnimating];
                 
                 
                // [self loadData];
                 
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hey!" message:@"User added successfully." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                 
                 [alert show];
                 
                 alert = nil;
                 
                 
             }
             else
             {
                 self.window.userInteractionEnabled = TRUE;
                 [actView stopAnimating];
                 [actView removeFromSuperview];
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hey!" message:[jsonObject valueForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                 [alert show];
             }
             
         }];

    }
    
}

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}



- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(NSMutableArray *)getUserList
{
    
    NSMutableArray *arr = [[NSMutableArray alloc] initWithCapacity:0];
    
    NSString *returnString;
    
    @try
	{
        
        NSString *postString =[NSString stringWithFormat:@"username=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"username"]];
        
        //NSLog(@"%@",[postString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]);
        
        NSURL *aUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/userlist",jsonURL]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:aUrl
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:60.0];
        
        
        
        
        
        [request setHTTPMethod:@"POST"];
        
        
        NSData *parameterData = [postString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        
        
        
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[parameterData length]];
        
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:parameterData];
        
        NSURLResponse *response = nil;
        NSError *err = nil;
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse: &response error: &err];
        
        returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        
        NSMutableArray *array = [NSJSONSerialization JSONObjectWithData:returnData options:NSJSONReadingMutableLeaves error:nil];
        
        if([[array valueForKey:@"status"] isEqualToString:@"1"])
        {
            for (NSDictionary *item in [array valueForKey:@"message"])
            {
                if([[item valueForKey:@"status"] isEqualToString:@"active"])
                {
                    [arr addObject:item];
                }
            }
            
            return arr;
        }
        else
        {
            return 0;
        }
        
    }
    @catch (NSException *e)
	{
		return nil;
	}
    
    
    return arr;
}

-(NSMutableArray *)getBlockedUserList
{
    
    NSMutableArray *arr = [[NSMutableArray alloc] initWithCapacity:0];
    
    NSString *returnString;
    
    @try
	{
        
        NSString *postString =[NSString stringWithFormat:@"username=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"username"]];
        
        //NSLog(@"%@",[postString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]);
        
        NSURL *aUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/userlist",jsonURL]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:aUrl
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:60.0];
        
        
        
        
        
        [request setHTTPMethod:@"POST"];
        
        
        NSData *parameterData = [postString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        
        
        
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[parameterData length]];
        
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:parameterData];
        
        NSURLResponse *response = nil;
        NSError *err = nil;
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse: &response error: &err];
        
        returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        
        NSMutableArray *array = [NSJSONSerialization JSONObjectWithData:returnData options:NSJSONReadingMutableLeaves error:nil];
        
        if([[array valueForKey:@"status"] isEqualToString:@"1"])
        {
            for (NSDictionary *item in [array valueForKey:@"message"])
            {
                if([[item valueForKey:@"status"] isEqualToString:@"blocked"])
                {
                    [arr addObject:item];
                }
            }
            
            return arr;
        }
        else
        {
            return 0;
        }
        
    }
    @catch (NSException *e)
	{
		return nil;
	}
}



#pragma mark - Remote Notifications Methods

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)devToken
{
    strDeviceToken = [[[devToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    //[strDeviceToken retain];
    
    NSLog(@"T-%@", strDeviceToken);
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
	NSLog(@"application:didFailToRegisterForRemoteNotificationsWithError: %@", error);
    
    strDeviceToken = @"99b8e921c7fc4483199190ea3f9c40154645aea7552e236e934b297200145b34";
    
    //[strDeviceToken retain];
    
	if ([error code] != 3010) // 3010 is for the iPhone Simulator
	{
		// show some alert or otherwise handle the failure to register.
	}
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo NS_AVAILABLE_IOS(3_0)
{
    
    NSLog(@"%@",userInfo);
    
}


#pragma mark Activity Indicator Methods -

- (void)startActivityIndicator:(UIView *)view

{
    
    if(self.activitityIndicator)
        
        [self.activitityIndicator removeFromSuperview];
    
    [self setActivitityIndicator:nil];
    
    self.activitityIndicator = [[MBProgressHUD alloc] initWithView:view];
    
    [view addSubview:self.activitityIndicator];
    
    self.activitityIndicator.dimBackground = NO;
    
    self.activitityIndicator.labelText = @"Please wait...";
    
    [self.activitityIndicator show:YES];
    
}


- (void)stopActivityIndicator

{
    
    // Regiser for HUD callbacks so we can remove it from the window at the right time
    
    [self.activitityIndicator removeFromSuperview];
    
    if(self.activitityIndicator)
        
        [self setActivitityIndicator:nil];
    
}



@end
