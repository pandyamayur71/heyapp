//
//  whatIsHeyVC.h
//  Hey.
//
//  Created by Tapan Nathvani on 12/10/14.
//  Copyright (c) 2014 Mayur Pandya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface whatIsHeyVC : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextView *txtView;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;

@end
