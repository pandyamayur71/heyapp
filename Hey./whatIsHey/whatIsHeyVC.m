//
//  whatIsHeyVC.m
//  Hey.
//
//  Created by Tapan Nathvani on 12/10/14.
//  Copyright (c) 2014 Mayur Pandya. All rights reserved.
//

#import "whatIsHeyVC.h"

@interface whatIsHeyVC ()

@end

@implementation whatIsHeyVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navBar"] forBarMetrics:UIBarMetricsDefault];
    
    self.title = @"Settings";
    
    UILabel *nav_titlelbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200,40)];
    nav_titlelbl.text=self.navigationItem.title;;
    nav_titlelbl.textColor=[UIColor whiteColor];
    nav_titlelbl.textAlignment=NSTextAlignmentCenter;
    nav_titlelbl.backgroundColor =[UIColor clearColor];
    nav_titlelbl.adjustsFontSizeToFitWidth=YES;
    UIFont *lblfont=[UIFont systemFontOfSize:25];
    [nav_titlelbl setFont:lblfont];
    self.navigationItem.titleView=nav_titlelbl;
    
    UIImage *menuImage = [UIImage imageNamed:@"btnBack.png"];
    UIButton *face = [UIButton buttonWithType:UIButtonTypeCustom];
    [face addTarget:self action:@selector(btnBack_clicked) forControlEvents:UIControlEventTouchUpInside];
    face.bounds = CGRectMake( 0, 0, menuImage.size.width, menuImage.size.height );
    [face setImage:menuImage forState:UIControlStateNormal];
    UIBarButtonItem *faceBtn = [[UIBarButtonItem alloc] initWithCustomView:face];
    
    self.navigationItem.leftBarButtonItem = faceBtn;
    
    
    self.scrollView.contentSize = CGSizeMake(320,self.txtView.frame.size.height + self.imgView.frame.size.height);

    // Do any additional setup after loading the view from its nib.
}

-(void)btnBack_clicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = FALSE;
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
